# translation of swfdec-gnome.HEAD.po to 简体中文
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Shaodong Di <gnuyhlfh@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: swfdec-gnome.HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-02-21 14:09+0000\n"
"PO-Revision-Date: 2008-02-23 19:40+0800\n"
"Last-Translator: Shaodong Di <gnuyhlfh@gmail.com>\n"
"Language-Team: 简体中文\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: ../data/swfdec-player.desktop.in.h:1
msgid "Play Adobe Flash files"
msgstr "播放 Adobe Flash 文件"

#: ../data/swfdec-player.desktop.in.h:2 ../player/swfdec-player.c:70
#: ../player/swfdec-player.ui.h:9
msgid "Swfdec Flash Player"
msgstr "Swfdec Flash 播放器"

#: ../player/swfdec-player.c:63
msgid "don't play sound"
msgstr "不要播放声音"

#: ../player/swfdec-player.c:64
msgid "start player paused"
msgstr "开始播放器暂停"

#: ../player/swfdec-player.c:65
msgid "[FILE ...]"
msgstr "[文件...]"

#: ../player/swfdec-player.c:79
#, c-format
msgid "Error parsing command line arguments: %s\n"
msgstr "解析命令行参数时错误：%s\n"

#: ../player/swfdec-player.ui.h:1
msgid "Close"
msgstr "关闭"

#: ../player/swfdec-player.ui.h:2
msgid "Mute"
msgstr "静音"

#: ../player/swfdec-player.ui.h:3
msgid "Mute sound output"
msgstr "静音"

#: ../player/swfdec-player.ui.h:4
msgid "Open _Recent"
msgstr "最近打开(_R)"

#: ../player/swfdec-player.ui.h:5
msgid "Open a file"
msgstr "打开文件"

#: ../player/swfdec-player.ui.h:6
msgid "Play / P_ause"
msgstr "播放/暂停(_A)"

#: ../player/swfdec-player.ui.h:7
msgid "Play or pause the file"
msgstr "播放或暂停播放文件"

#: ../player/swfdec-player.ui.h:8
msgid "Select a recently opened file"
msgstr "选择最近打开的文件"

#: ../player/swfdec-player.ui.h:10
msgid "_About"
msgstr "关于(_A)"

#: ../player/swfdec-player.ui.h:11
msgid "_Close"
msgstr "关闭(_F)"

#: ../player/swfdec-player.ui.h:12
msgid "_File"
msgstr "文件(_F)"

#: ../player/swfdec-player.ui.h:13
msgid "_Help"
msgstr "帮助(_H)"

#: ../player/swfdec-player.ui.h:14
msgid "_Open..."
msgstr "打开(_O)..."

#: ../player/swfdec-window.c:85
#, c-format
msgid "<i>%s</i> is broken, playback aborted."
msgstr "<i>%s</i> 已损坏，放弃播放。"

#: ../player/swfdec-window.c:87
#, c-format
msgid "<i>%s</i> is not a Flash file."
msgstr "<i>%s</i> 不是 Flash 文件。"

#: ../player/swfdec-window.c:240
msgid "Broken user interface definition file"
msgstr "用户界面定义文件已损坏"

#: ../player/swfdec-window-handlers.c:53
msgid "Select a file to play"
msgstr "选择要播放的文件"

#. Translators: This is a special message that shouldn't be translated
#. * literally. It is used in the about box to give credits to
#. * the translators.
#. * Thus, you should translate it to your name and email address.
#. * You should also include other translators who have contributed to
#. * this translation; in that case, please write each of them on a separate
#. * line seperated by newlines (\n).
#.
#: ../player/swfdec-window-handlers.c:131
msgid "translator-credits"
msgstr "Shaodong Di <gnuyhlfh@gmail.com>"

#: ../thumbnailer/swfdec-thumbnailer.schemas.in.h:1
msgid "Enable the creation of new thumbnails."
msgstr "启用创建新缩略图"

#: ../thumbnailer/swfdec-thumbnailer.schemas.in.h:2
msgid "Enable thumbnailing of Flash files"
msgstr "启用 Flash 文件预览"

#: ../thumbnailer/swfdec-thumbnailer.schemas.in.h:3
msgid "Thumbnail command for Flash files"
msgstr "Flash 文件的预览命令"

#: ../thumbnailer/swfdec-thumbnailer.schemas.in.h:4
msgid ""
"Valid command plus arguments for the Flash file thumbnailer. See nautilus "
"thumbnailer documentation for more information."
msgstr ""
"有效的 Flash 文件预览命令及参数。请参考 nautilus thumbnailer 文档以获取更多信"
"息。"

#~ msgid ""
#~ "Boolean options available, true enables thumbnailing and false disables "
#~ "the creation of new thumbnails"
#~ msgstr "布尔选项可用，选真启用预览，选假禁止生成预览。"
